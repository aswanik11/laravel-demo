<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Models\Media;
use Validator;

class MediaController extends BaseController
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();

        $validator = Validator::make($request->all(), [
            // 'avatar' => 'required'
            'avatar' => 'required|image|mimes:jpeg,png,jpg|max:500',
        ]);

        $imageName['avatar'] = time() . '.' . $request->avatar->extension();

        if ($validator->fails()) {
            return $this->sendError('Validation Error.', $validator->errors());
        }

        $request->avatar->move(public_path('images'), $imageName['avatar']);
        $media = Media::create($imageName);

        return $this->sendResponse($imageName, 'File uploaded successfully.');
    }
}
